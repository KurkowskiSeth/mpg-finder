# MPG Finder

## Seth Kurkowski

### Description

MPG Finder is a companion app that will allow the user to enter the for each time they fill up their car with gas: the miles their car has, how much gas they put in, the cost of the gas and the octane of the gas and the app will give the user the average cost per gallon, miles per gallon for each type of fuel.